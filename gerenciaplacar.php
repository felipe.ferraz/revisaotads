<?php
include_once "conecta.php";

if (isset($_POST['cadastrar'])){
    /********* RESPONDA AQUI A QUESTÃO 03 - A  ********/

    $idTimeA = $_POST['idtimeA']?$_POST['idtimeA']:'';
    $idTimeB = $_POST['idtimeB']?$_POST['idtimeB']:'';
    $golsA = $_POST['golsA']?$_POST['golsA']:'';
    $golsB = $_POST['golsB']?$_POST['golsB']:'';

    $query = ' INSERT INTO `prova`.`jogos`
        (
        `idtimeA`,
        `idtimeB`,
        `golsA`,
        `golsB`)
        VALUES
        (
        ?,
        ?,
        ?,
        ?);
        ';

    $stmt = $db->prepare($query);
    $stmt->bind_param('iiii', $idTimeA, $idTimeB, $golsA, $golsB);

    if(!$stmt->execute()){
        die('Erro na inserção');
    }



    /*********************************************/
}
?>


<!doctype html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <title>Gerenciar Placar</title>
    <link rel="stylesheet" type="text/css" href="css/prova.css">
    <link rel="stylesheet" href="css/font-awesome.css">
</head>
<body>

<form class="form" action="" method="post">
    <legend class="header">Cadastrar Placar:</legend>

    <select name="idtimeA" class="selectbox">
        <?php
        /********* RESPONDA AQUI A QUESTÃO 03 - B  ********/
            $query = 'SELECT * FROM `prova`.`times`;';
            if($result = $db->query($query)) {
                while ($row = $result->fetch_assoc()) : ?>
                    <option value="<?= $row['idtime'] ?>"><?= $row['nome'] ?></option>
                    }
                <?php
                endwhile;
            }
            ?>
        /****************** continua.. ********************/
        ?>
    </select>
    <input type="text" placeholder="Gols" class="golbox" name="golsA" required >

    <span class="text">X</span>

    <select name="idtimeB" class="selectbox">
        /********* CONTINUE A RESPONDER A QUESTÃO 03 - B  ********/
        <?php
        /********* RESPONDA AQUI A QUESTÃO 03 - B  ********/
        $query = 'SELECT * FROM `prova`.`times`;';
        if($result = $db->query($query)) {
            while ($row = $result->fetch_assoc()) : ?>
                <option value="<?= $row['idtime'] ?>"><?= $row['nome'] ?></option>
                }
            <?php
            endwhile;
        }
        ?>
        /********************************************************/
    </select>
    <input type="text" placeholder="Gols" class="golbox" name="golsB" required >

    <input type="submit" name="cadastrar" value="Cadastrar" class="button">

</form>
<a class="text" href="index.php">Voltar</a>
</body>
</html>
