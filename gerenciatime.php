<?php
include_once "conecta.php";

if (isset($_POST['nome'])){
    /********* RESPONDA AQUI A QUESTÃO 02 - A  ********/

   $query = ' INSERT INTO `prova`.`times`
(`nome`)
VALUES
(?);';

   $stmt = $db->prepare($query);
   $stmt->bind_param('s', $_POST['nome']);

   if(!$stmt->execute()){
       die('Erro na inserção');
   }




    /*********************************************/
}else if (isset($_GET['excluir'])){
    /********* RESPONDA AQUI A QUESTÃO 02 - B  ********/
    $query = ' DELETE FROM `prova`.`times`
    WHERE idtime = ?;
    ';

    $stmt = $db->prepare($query);
    $stmt->bind_param('i', $_GET['excluir']);

    if(!$stmt->execute()){
        die('Erro na exclusão');
    }



    /*********************************************/
}
?>

<!doctype html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <title>Gerenciar Time</title>
    <link rel="stylesheet" type="text/css" href="css/prova.css">
    <link rel="stylesheet" href="css/font-awesome.css">
</head>
<body>

<form class="form" action="" method="post">
        <legend class="header">Cadastrar Time:</legend>
        <input type="text" placeholder="Nome" class="textbox" name="nome" required >
        <input type="submit" name="cadastrar" value="Cadastrar" class="button">
</form>

<table>
    <thead>
    <tr>
        <th colspan="3">Times</th>
    </tr>
    </thead>
    <tbody>
<?php
    try {
        $querySEL = 'SELECT * FROM `times`';
        $stmt = $db->query($querySEL);
         foreach ($stmt as $row):
?>
            <tr>
                <td><?= $row['idtime'] ?></td>
                <td><?= $row['nome'] ?></td>
                <td>
                    <a href="gerenciatime.php?excluir=<?= $row['idtime'] ?>"> <i class="fa fa-trash button2 excluir"></i></a>
                </td>
            </tr>
<?php
         endforeach;
    }catch( PDOException $Exception ) {
        throw new MyDatabaseException( $Exception->getMessage( ) , $Exception->getCode( ) );
    }
?>
    </tbody>
</table>
<a class="text" href="index.php">Voltar</a>
</body>
</html>
